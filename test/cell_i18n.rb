require "cutest"
require "mocoso"
require_relative "../lib/cell_i18n"

include Mocoso

module Page
  # Let's pretend these are `Trailblazer::Cell`s
  class Header
    include CellI18n

    def self.call
      new
    end
  end

  class Body
    include CellI18n

    def self.call
      new
    end
  end
end

scope "namespace" do
  test "honours the namespace of the class" do
    assert_equal ["Page", "Header"], Page::Header.().send(:namespace)
    assert_equal ["Page", "Body"], Page::Body.().send(:namespace)
  end
end

scope "translation lookup" do
  test "the default file path is lib/locale" do
    assert_equal "lib/locale", Page::Header.().i18n_file_path
  end

  test "the default locale is en" do
    assert_equal "en", Page::Header.().i18n_locale
  end

  test "the default parser is JSON" do
    parser, extension = Page::Header.().i18n_parser

    assert_equal JSON.method(:parse), parser
    assert_equal "json", extension
  end

  test "searches for translations lib/locale/*.json files" do
    file = <<-EOF
    {
      "Page": {
        "Header": {
          "foo": "foo in english"
        }
      }
    }
    EOF

    expect(File, :read, with: ["lib/locale/en.json"], return: file) do
      assert_equal "foo in english", Page::Header.().i18n("foo")
    end
  end

  test "can use YAML as well" do
    file = <<-EOF
    Page:
      Header:
        foo: "foo in english"
    EOF

    expect(File, :read, with: ["lib/locale/en.yml"], return: file) do
      class Page::Header
        require "yaml"

        def i18n_parser
          [YAML.method(:load), "yml"]
        end
      end

      assert_equal "foo in english", Page::Header.().i18n("foo")
    end
  end
end
