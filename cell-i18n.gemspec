Gem::Specification.new do |s|
  s.name = "cell-i18n"
  s.version = "0.1.0"
  s.summary = "i18n library inspired in Trailblazer's Cell"
  s.description = s.summary
  s.authors = ["Lucas Tolchinsky"]
  s.email = ["tonchis@protonmail.com"]
  s.homepage = "https://gitlab.com/tonchis/cell-i18n"
  s.license = "MIT"

  s.files = `git ls-files`.split("\n")

  s.add_development_dependency "dep", ">= 1.1.0"
  s.add_development_dependency "cutest", ">= 1.2.3"
  s.add_development_dependency "mocoso", ">= 1.2.3"
end

