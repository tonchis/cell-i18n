require "json"

module CellI18n
  def i18n(key, locale = i18n_locale)
    translation_path = namespace + [key]
    dictionaries[locale].dig(*translation_path)
  end

  def i18n_file_path
    "lib/locale".freeze
  end

  def i18n_locale
    "en".freeze
  end

  def i18n_parser
    [JSON.method(:parse), "json".freeze]
  end

  private def namespace
    self.class.name.split("::".freeze)
  end

  private def dictionaries
    @@dictionaries ||= begin
      hash = Hash.new do |hash, locale|
        parser, extension = i18n_parser
        dictionary = parser.(File.read(File.join(i18n_file_path, "#{locale}.#{extension}")))
        hash[locale] = dictionary
      end
    end
  end
end
